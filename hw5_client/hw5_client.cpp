
/*  Programmer: Brandon Campbell
 *  Date:       1/25/2018 - 4/17/2018
 *  Project:    Homework 5
 *  Purpose:    Demonstrates creating multiple threads with multiple processes from a single parent. Also
 *              still implements one forked process. Performs Logging, Creating Files, Searching for Files,
 *              and counting the number of key files in a folder. This is done via a client-server architecture.
 *              This is the client program, which will read the commandfile and pass the commands to the
 *              server program.
 */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Queue library
#include <queue>

// Unix library
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fstream>

#include "Message.h"

// Shortcut for the std namespace
using namespace std;
using std::queue;
// String length, plus one for \0
#define STRLEN 32

int openCommandFile(string cmdFile);
int readCmdFile(Message &msg);


fstream cmdStream; // Used for reading in the command file.
int msgCounter;


//
// main( )
//
// Creates 4 threads and 1 forked process in order to demonstrate threading and forking.
// Performs creating files, reading files, and counting the numbers of files.
//
int main( int argc, char** argv )
{
    //SetupData setupData;
    string dataString;
    string strPortNumber;
    string strServerName;
    string strCommandFile;
    char* tempMsgString;
    int setupDataIsPresent;
    //The following serve as the communication pipes for the processes to pass messages.
    Message msg;    //a message struct that will hold messages as they are read in.

    int mainSocket, connection;
    struct addrinfo* addressInformation;
    char address[40], portNum[10];

    //holds the path and name of the setupfile.
    char portNumber[STRLEN] = "", serverName[STRLEN] = "", commandFileName[STRLEN] = "";
    // For return value of getopt( )
    char argFlag;


    // Use getopt( ) to loop through -c (command file name) ,-s (server name), -p (port number) flags
    while ( (argFlag = getopt(argc, argv, "c:s:p:")) != -1 )
    {
        switch (argFlag)
        {
            case 'c': strncpy(commandFileName, optarg, STRLEN-1);
                break;
            case 's': strncpy(serverName, optarg, STRLEN-1);
                break;
            case 'p': strncpy(portNumber, optarg, STRLEN-1);
                break;
            default:
                break;
        }; // switch
    } // while

    strCommandFile = commandFileName;

    //instantiate these strings to properly pass in strings to the overloaded constructor.

    //if we got a path in the arguments, put it in the SetupData
    if (!strlen(commandFileName) > 0 || !strlen(serverName) > 0 || !strlen(portNumber) > 0)
    {
        cout << "Usage: hw5 -c <commandFileName> -s <serverName> -p <portNumber>" << endl;
        //exit(1);
    }

    strcpy(portNum, portNumber);
    strcpy(address, serverName);


    //attempt to open the command file and load in its data...
    if (openCommandFile(commandFileName) == 1)
    {
        //try to open all of the pipes, if error, let user know.


        bool notQuit = true;    //this keeps track of whether or not we've read a 'Q'
        char setupDataToSend[200];


        //We've made it this far, go ahead and setup the socket and connection to the server.
        mainSocket = socket(AF_INET, SOCK_STREAM, 0);
        if(mainSocket < 0)
        {
            cout << "Error creating socket" << endl;
            exit(0);
        }
        // Get the address
        if(getaddrinfo(address, portNum, NULL, &addressInformation) != 0)
        {
            cout << "Error getting address" << endl;
            exit(0);
        }
        // Set up the connection
        connection = connect(mainSocket, addressInformation->ai_addr, addressInformation->ai_addrlen);
        if(connection < 0)
        {
            cout << "Connection failed. Exiting program.";
            exit(0);
        }

        cout << "Client is now connected to " << address << " on port " << portNumber << endl;

        while(cmdStream.peek() != -1) //check to see if we've reached the end of the file and we haven't gotten a 'Q' msg.
        {
            if (readCmdFile(msg) == 1) //if there's a command to be read, read it and store it in msg
            {
                string childType;
                write(mainSocket, (char*)&msg, sizeof(Message) ); // Send a message to the Server.
                read(mainSocket, (char*)&msg, sizeof(Message) ); // Get a message from the Server.
                switch (msg.command)
                {
                    case 'N':
                        childType = "Number";
                        break;
                    case 'P':
                        childType = "Put";
                        break;
                    case 'S':
                        childType = "Search";
                        break;
                    case 'Q':
                        close(mainSocket);
                        cout << "Client has now disconnected from " << address << " on port " << portNumber << endl;
                        cmdStream.close();
                        exit(0);
                    default:
                        break;

                }
                // Report the received message to the console.
                cout << "Client received message from " << childType << "Thread: Msg #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
            }
        }
    }
    else    //of not found/able to read, print an error msg. to console and to the logfile.
    {
        cout << "Could not find/read the command file: \"" + strCommandFile + "\"" << endl;
    }
}



/*
 * openCommandFile()
 * Opens the directory that is listed in pathName, then reads the setupfile located there
 */
int openCommandFile(string cmdFilePath)
{
    int returnValue = 0;

    cmdStream.open(cmdFilePath, ios::in);
    if (!cmdStream)  //otherwise we couldn't find/open the file, return 0.
    {
        returnValue = 0;
    } else    //if the file opens, it must be present, return 1.
    {
        returnValue = 1;
    }

    return returnValue;
}

/*
* read()
* Reads the data within the setup file that is currently in the filestream
*/
int readCmdFile(Message &msg)
{
    string line;    //holds the temporary string that is received from the getline

    //first check to see if the setupdata is in the standard format: i.e. logfile: mylog, if so
    //parse with a normal cin style

    getline(cmdStream, line);

    if(!line.empty()) //check for an empty line--if line is occupied, read triplet's contents.
    {
        msg.id = ++msgCounter;
        msg.command = char(line[0]);

        getline(cmdStream, line);
        strcpy(msg.key, line.c_str());

        getline(cmdStream, line);
        strcpy(msg.payload, line.c_str());

        return 1; //return 1 for success.
    } else
        return 0; //return 0 for a blank line--no content read.
}

