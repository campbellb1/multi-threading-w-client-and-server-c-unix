//
// Created by brandon on 4/18/18.
//

#ifndef HW5_SERVER_SAFEQUEUE_H
#define HW5_SERVER_SAFEQUEUE_H
#include <iostream>

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Queue library
#include <queue>

// Unix library
#include <pthread.h>
#include "Message.h"
#include <semaphore.h>
// Queue library
#include <queue>
#include <unistd.h>
#include <dirent.h>
using namespace std;


class SafeQueue
{
public:
    //CONSTRUCTORS
    SafeQueue();

    void enqueue(Message msg);
    Message dequeue();

private:
    queue<Message> mainQueue;
    pthread_mutex_t mainLock;
    sem_t mainSemaphore;
};


#endif //HW5_SERVER_SAFEQUEUE_H
