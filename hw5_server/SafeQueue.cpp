/**
 * Filename:    SafeQueue.cpp
 * Date:        4/19/2018
 * Purpose:     The purpose of this file is to encapsulate the STL queue for messages. Adds
 *              mutex protection to pushing and popping messages onto the queues.
 */

#include "SafeQueue.h"
#include "Message.h"
#include <semaphore.h>
// Queue library
#include <queue>
#include <unistd.h>
#include <dirent.h>
#include <pthread.h>

using namespace std;

//Its constructor should initialize the mutex to unlocked and the semaphore to 0.
SafeQueue::SafeQueue()
{
    mainLock = PTHREAD_MUTEX_INITIALIZER;
    sem_init(&mainSemaphore, 0, 0);
}


/**
 * pushOnQueue(Message, pthread_mutex_t*, sem_t*, queue<Message>*)
 * Purpose: Lock the passed in mutex and push a passed in message onto the passed in queue. Once the
 *          Message has been pushed onto the Queue, unlock the mutex and increment the Semaphore.
 * @param pushMsg - the message that will receive whatever is "popped off" the queue
 * @param mutex - the lock for the passed in queue
 * @param semaphore - our counting semaphore to keep track of how many items are still on the queue
 * @param pushQueue - the queue that we are pushing items onto
 */
void SafeQueue::enqueue(Message pushMsg)
{
    pthread_mutex_lock(&mainLock); // Locks the critical part of the code for the specified queue/thread.
    mainQueue.push(pushMsg); // Pushes the message on the passed in queue.
    pthread_mutex_unlock(&mainLock); // Unlock the critical part of the code for the specified queue/thread.
    sem_post(&mainSemaphore);    // Increment our counting semaphore by one.
}

/**
 *  popOffQueue(Message, pthread_mutex_t*, sem_t*, queue<Message>*)
 *  Purpose: Wait until we can access the passed in queue by using the passed in semaphore. Lock the
 *           passed in mutex until the critical code--popping off the queue--is finished.
 * @param popMsg - the message to receive whatever is popped off the Queue.
 * @param mutex  - the lock for the passed in Queue
 * @param semaphore - our counting semaphore to keep track of how many items are on the queue
 * @param popQueue - the queue that we are popping items from
 */
Message SafeQueue::dequeue()
{
    Message msgToReturn;
    sem_wait(&mainSemaphore);    // Increment the semaphore--as we are going to put something on the queue.
    pthread_mutex_lock(&mainLock); // Lock the critical part of the code for the specified queue/thread.
    msgToReturn = mainQueue.front(); // Remove a message from the queue and put it on our message.
    mainQueue.pop();        // Reduce the queue pointer.
    pthread_mutex_unlock(&mainLock); //Unlock the critical code for the specified queue/thread.

    return msgToReturn;
}