/*                                          */
/* Programmer: Brandon Campbell             */
/* Date  : 1/23/2018                        */
/*                                          */
/* Purpose: Handles all the functionality   */
/*          of the SetupData class--taking  */
/*                                          */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Unix library
#include <unistd.h>
#include "SetupData.h"
// Shortcut for the std namespace
using namespace std;

/*
 * SetupData()
 * Default Constructor for SetupData class.
 */
SetupData::SetupData()
{

}

/*
 * Single argument Constructor
 * Takes in the passed in pathname and adds it to our Map.
 */
SetupData::SetupData(const string &pathname)
{
    values["pathname:"] = pathname;
}

/*
 * Constructor-
 * SetupData(string, string)
 * Constructor that takes in the path of the setupfunction. Also
 * optionally takes in the name of the setupfile--default name is
 * setupfile.
 */
SetupData::SetupData(const string &pname, const string &sname)
{
    if(sname.empty())
    {
        setSetupfilename("setupfile");
    }
    else
    {
        setSetupfilename(sname);
    }

    if(pname.empty())
    {
        cout << "Usage: hw1 -p <pathname> [-s <setupfilename>]";
        exit(1);
    }
    else
    values["pathname"] = pname;
}

/*
 * close()
 * Simply closes the filestream.
 */
void SetupData::close()
{
    f.close();
}

/*
 * error(int)
 */
string SetupData::error(int e)
{
    switch(e)
        default:
            return "An error happened";
}

/*
 * getCommandfilename()
 * This method gets and returns the name of the command file--i.e. the value in our map under the key - logfile:
 */
string SetupData::getCommandfilename()
{
    return values.find("commandfile")->second;
}


/*
 * getLogfilename()
 * This method gets and returns the name of the logfile--i.e. the value in our map under the key - logfile
 */
string SetupData::getLogfilename()
{
    return values.find("logfile")->second;
}

/*
 * getPathname()
 * This method gets and returns the name of the pathname--i.e. the value in our map under the key - pathname
 */
string SetupData::getPathname()
{
    return values.find("pathname")->second;
}

/**
 * getPortNumber()
 * This method gets and returns the name of the portnumber--i.e. the value in our map under the key - portnumber
 * @return the port number held in the Map.
 */
string SetupData::getPortNumber()
{
    return values.find("portnumber")->second;
}

/*
 * getSetupfilename
 * This method simply returns whatever we have stored in our map at key - setupfilename
 */
string SetupData::getSetupfilename()
{
    return values.find("setupfilename")->second;
}

/*
 * getUsername
 * This method simply returns the name of the username stored in our map at key - username
 */
string SetupData::getUsername()
{
    return values.find("username")->second;
}


/*
 * open()
 * Opens the directory that is listed in pathName, then reads the setupfile located there
 */
int SetupData::open()
{
    int returnValue = 0;
    if(chdir(values["pathname"].c_str()) == -1)
    {
        returnValue = 0;
    }
else
    {
        f.open(values["setupfilename"].c_str(), ios::in);
        if (!f)  //otherwise we couldn't find/open the file, return 0.
        {
            returnValue = 0;
        } else    //if the file opens, it must be present, return 1.
        {
            returnValue = 1;
        }
    }
    return returnValue;
}

/*
 * print()
 * Prints the data that we have read from the SetupFile
 */
void SetupData::print()
{
    cout << "pathname: " << values["pathname"] << endl;
    cout << "setupfilename: " << values["setupfilename"] << endl;
    cout << "logfile: " << values["logfile"] << endl;
    cout << "commandfile: " << values["commandfile"] << endl;
    cout << "username: " << values["username"] << endl;
}

/*
 * read()
 * Reads the data within the setup file that is currently in the filestream
 */
void SetupData::read()
{
    string key;
    string value;
    string line;
    string firstArg;
    string secondArg;

    //first check to see if the setupdata is in the standard format: i.e. logfile: mylog, if so
    //parse with a normal cin style
    while(f >> key >> value)
    {
        //if we catch a non-normal setupfile: i.e. logfile:mylog, parse using getline split at the colon.
        if(value.find(':')!=std::string::npos)
        {
            //we already have the first two lines of values, so go ahead and put those in.
            firstArg = key.substr(0,key.find(":"));
            secondArg = key.substr(key.find(":") + 1, strlen(key.c_str()));
            values[firstArg] = secondArg;

            firstArg = value.substr(0,value.find(":"));
            secondArg = value.substr(value.find(":") + 1, strlen(value.c_str()));
            values[firstArg] = secondArg;
            getline(f, line);   //this is needed to clear the stream of a blank.
            //now go ahead and get the rest of the data.
            while(!f.eof())
            {
                getline(f, line);
                if(f.eof())
                    break;
                firstArg = line.substr(0,line.find(":"));
                secondArg = line.substr(line.find(":") + 1, strlen(line.c_str()));
                values[firstArg] = secondArg;
            }
            break;
        }

        values[key.substr(0, key.size()-1)] = value;
    }


}

/*
 * setCommandfilename(string)
 * This method simply adds the passed in commandfile name to our
 * Map object under the "key" - commandfile
 */
void SetupData::setCommandfilename(string cname)
{
    values["commandfile"] = cname;
}

/*
 * setLogfile(string)
 * This method simply adds the passed in logfile name to our
 * Map object under the "key" - logfile:.
 */
void SetupData::setLogfile(string lname)
{
    values["logfile"] = lname;
}

/*
 * setPathname(string)
 * This method simply adds the passed in pathname to our
 * Map object under the "key" - pathname.
 */
void SetupData::setPathname(string pname)
{
    values["pathname"] = pname;
}

/**
 * setPortNumber(string)
 * This method simply adds the passed in port number to our
 * Map object under the key - portnumber.
 * @param port
 */
void SetupData::setPortNumber(string port)
{
    values["portnumber"] = port;
}

/*
 * setSetupfilename(string)
 * This method simply adds the passed in setupfilename to our
 * Map object under the "key" - setupfilename.
 */
void SetupData::setSetupfilename(string sname)
{
    values["setupfilename"] = sname;
}

/*
 * setUsername(string)
 * This method simply adds the passed in username to our
 * Map object under the "key" - username
 */
void SetupData::setUsername(string uname)
{
    values["username"] = uname;
}


