/**
 * Filename:    hw5_server.cpp
 * Date:        4/19/2018
 * Purpose:     The purpose of this file is to handle requests from a client program that gives
 *              the servers commands. These commands are executed in this file via the Put,
 *              Number, Search threads. The Return Thread sends return messages back to the
 *              Client.
 */

#include <iostream>

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Queue library
#include <queue>

// Unix library
#include <unistd.h>
#include <dirent.h>


#include "SetupData.h"
#include "Log.h"
#include "Message.h"
#include <wait.h>
#include <pthread.h>
#include <semaphore.h>
#include "SafeQueue.h"

#include <sys/socket.h>
#include <netdb.h>

int openCommandFile(string cmdFile);
int readCmdFile(Message &msg);
void createLogger(int loggerPipe[], Message msg);
void * createNumber(void * p);
void * createPut(void * p);
void * createReturn(void * p);
void * createSearch(void * p);
void initializePThreads();
int openPipes(int loggerPipe[]);
void closePipes(int loggerPipe[]);

void initSemaphores();

#define STRLEN 32

// Go ahead and pre-create 4 messages for the children to send as messages.
string duplicateMsg = "Duplicate";
string okayMsg = "Okay";
string notFoundString = "Not Found";
string errorMessage = "Error!";

fstream cmdStream;
int msgCounter;
Log log;

// Instantiate the 4 pthreads for each of the needed threads.
pthread_t putThread;
pthread_t searchThread;
pthread_t numberThread;
pthread_t returnThread;

// Instantiate the 4 SafeQueue for our threads.
SafeQueue putQueue;
SafeQueue searchQueue;
SafeQueue numberQueue;
SafeQueue returnQueue;

int connection;     // Holds the connection to the client.

int main(int argc, char** argv)
{
    Message inboundMsg;
    int serverSocket;
    char portnum[10];
    struct addrinfo* myinfo; // System-defined struct
    int loggerPipe[2];
    Message msg;    // a message struct that will hold messages as they are read in.

    string dataString;
    string strPathToSetupFile;
    string strNameOfSetupFile;
    int setupDataIsPresent;

    char pathToSetupFile[STRLEN] = "", nameOfSetupFile[STRLEN] = "";
    // For return value of getopt( )
    char argFlag;

    openPipes(loggerPipe);

    //try to open all of the pipes, if error, let user know.
    if(openPipes(loggerPipe) == -1)
    {
        cout << "Error with logger pipe initialization!!";
        exit(1);
    }

    pid_t pid;  //will hold process id


        initializePThreads();
        // Use getopt( ) to loop through -p (path to find setupfile), -s (setupfilename -- optional) flags
        while ( (argFlag = getopt(argc, argv, "p:s:")) != -1 )
        {
            switch (argFlag)
            {
                case 'p': strncpy(pathToSetupFile, optarg, STRLEN-1);
                    break;
                case 's': strncpy(nameOfSetupFile, optarg, STRLEN-1);
                    break;
                default:
                    break;
            }; // switch
        } // while

        //instantiate these strings to properly pass in strings to the overloaded constructor.
        strPathToSetupFile = pathToSetupFile;
        strNameOfSetupFile = nameOfSetupFile;
        //if we got a path in the arguments, put it in the SetupData
        if (!strlen(pathToSetupFile) > 0)
        {
            cout << "Usage: hw5 -p <pathname> -s <setupDataFile>" << endl;
            //exit(1);
        }

        //generate a SetupData from the passed in arguments.
        SetupData setupData(strPathToSetupFile, strNameOfSetupFile);

        //if we got a setupfile name, put it in the SetupData
        if(strlen(nameOfSetupFile) > 0)
        {
            setupData.setSetupfilename(nameOfSetupFile);
        }
        else    //otherwise put the Default setupfile name in: "setupfile"
        {
            setupData.setSetupfilename("setupfile");
        }

        setupDataIsPresent = setupData.open();  //see if we can open the setupdata file. . .
        if(setupDataIsPresent == 1) //check if we could open the file--if not, write an error message.
        {
            setupData.read();
            //store the SetupFile data into a temporary string for writing to the logfile.
            setupData.close();

            if (!setupData.getLogfilename().empty()) //if the setupdata contains some logfile name, replace the default.
            {
                log.setLogfileName(setupData.getLogfilename());
            }

            if ((pid = fork()) < 0) //check for forking error
            {
                perror("Error on fork attempt!\n");
                exit(2);
            }

            if (pid == 0) //if pid == 0, it's a child
            {//------CHILD CODE-------
                size_t n = 0;
                size_t p = getpid();
                createLogger(loggerPipe, msg);//activate the Logger process

                exit(0);
            }
            else    //-------------PARENT CODE --------
            {



                char setupDataToSend[200];
                string stringSetupInfo1 = "pathname: " + setupData.getPathname() +
                                          ",setupfilename: " + setupData.getSetupfilename() +
                                          ",logfile: " + setupData.getLogfilename() +
                                          ",username: " + setupData.getUsername() +
                                          ",portnumber: " + setupData.getPortNumber() + ",";

                strcpy(setupDataToSend, stringSetupInfo1.c_str());


                write(loggerPipe[1], stringSetupInfo1.c_str(), 200);


                strcpy(portnum, setupData.getPortNumber().c_str());
                serverSocket = socket(AF_INET, SOCK_STREAM, 0);
                if(serverSocket < 0)
                {
                    cout << "Error creating socket" << endl; //
                    exit(0);
                }

                if(getaddrinfo("0.0.0.0", portnum, NULL, &myinfo) != 0) // Get the address.
                {
                    cout << "Error getting address" << endl;
                    exit(0);
                }

                if(bind(serverSocket, myinfo->ai_addr, myinfo->ai_addrlen) < 0) // Bind the address to the socket.
                {
                    cout << "Error binding to socket" << endl;
                    exit(0);
                }

                if(listen(serverSocket, 1) < 0) // Start to listen to the socket.
                {
                    cout << "Error in listen" << endl;
                    exit(0);
                }
                connection = accept(serverSocket, NULL, NULL); // set up the connection.
                if(connection < 0)
                {
                    cout << "Error in accept" << endl;
                    exit(0);
                }

                cout << "Server is now connected to address 0.0.0.0 on port " << setupData.getPortNumber() << endl;

                while (true)
                {
                    read(connection, (char *) &inboundMsg, sizeof(Message));

                    write(loggerPipe[1], (char *) &inboundMsg, sizeof(Message)); //go ahead and write to the Logger

                    switch (inboundMsg.command) {
                        case 'P':   //if we read a 'P', send the message to the Put Thread.
                            putQueue.enqueue(inboundMsg);
                            break;
                        case 'S':   //if we read an 'S', send the message to the Search Thread.
                            searchQueue.enqueue(inboundMsg);
                            break;
                        case 'N':   //if we read an 'N', send the message to the Number Thread.
                            numberQueue.enqueue(inboundMsg);
                            break;
                        case 'Q':   //If we read a 'Q', tell all the threads to quit.
                            putQueue.enqueue(inboundMsg);
                            searchQueue.enqueue(inboundMsg);
                            numberQueue.enqueue(inboundMsg);

                            //Wait for all of the Threads to die.
                            pthread_join(putThread, NULL);
                            pthread_join(searchThread, NULL);
                            pthread_join(numberThread, NULL);
                            pthread_join(returnThread, NULL);
                            waitpid(-1, NULL, 0);
                            closePipes(loggerPipe);
                            close(serverSocket);
                            cout << "Server is now disconnected from address 0.0.0.0, port " << portnum << endl;
                            exit(0);

                        default:
                            break;
                    }
                }
            }
        }
        else //if we were not able to read/find the setupdata file, write such in the logfile, tell user, and exit.
        {
            dataString = "Set up file " + setupData.getSetupfilename() + " does not exist!";
            log.open();

            log.writeLogRecord(dataString);

            log.close();
            cout << "Could not find/read the setupfile: \"" + setupData.getSetupfilename() + "\"" << endl;
            exit(1);
        }
    }



/*
 * closePipes(int[])
 * Purpose: Closes all pipes used in the program. In this case, it only closes the logger pipes.
 */
void closePipes(int loggerPipe[])
{
    int returnInt = 0;  //This will tell us if the pipe opening failed or not.

    //Closes all the pipes
    close(loggerPipe[0]);
    close(loggerPipe[1]);
}


/*
 * createLogger(int[])
 * Purpose: Handles the functionality of the Logger child. Logs each message that is read by the
 *          main program.
 */
void createLogger(int loggerPipe[], Message msg)
{
    size_t pointer = 0;
    if(log.open() == -1)
    {
        cout << "failed to open logfile" << endl;
    }

    char incomingMessage[32];
    string strIncomingMessage;
    string token;
    string delimiter = ",";

    close(loggerPipe[1]);
    read(loggerPipe[0], incomingMessage, 200);
    strIncomingMessage = (string)incomingMessage;
    while ((pointer = strIncomingMessage.find(delimiter)) != std::string::npos)
    {
        token = strIncomingMessage.substr(0, pointer);
        log.writeLogRecord(token);
        strIncomingMessage.erase(0, pointer + delimiter.length());
    }


    /* Read in a string from the pipe */
    while(true)
    {
        close(loggerPipe[1]);
        read(loggerPipe[0], (char*)&msg, sizeof(Message));
        cout << "Log server received message #" << msg.id << ", key = " << msg.key << ", payload = " << msg.payload << endl;
        if(msg.command == 'Q')
        {
            log.close();
            exit(0);
        }
        log.writeLogRecord("Message ID: " + to_string(msg.id) + "\n");
        log.writeLogRecord("Command: " + string(1, msg.command) + "\n");
        log.writeLogRecord("Key: " + string(msg.key) + "\n");
        log.writeLogRecord("Payload: " + string(msg.payload) + "\n");
    }

}

/*
 * createNumber(int[], Message, int[])
 * Performs the process for the Number Child: counts the number
 * of "key files" in the current directory.
 */
void * createNumber(void * p)
{
    Message numberMsg;

    while(true)
    {
        numberMsg = numberQueue.dequeue();
        cout << "Number server received message #" << numberMsg.id << ", key = " << numberMsg.key << ", payload = " << numberMsg.payload << endl;
        if (numberMsg.command == 'Q')
        {
            returnQueue.enqueue(numberMsg);
            pthread_exit(0);
        }


        int file_count = 0;
        DIR * dirp;
        struct dirent * entry;

        dirp = opendir("./"); // Try to open the current directory to count the files.
        while ((entry = readdir(dirp)) != NULL)
        {
            file_count++;
        }
        closedir(dirp);

        file_count -= 5; // This is needed to account for files unrelated to key files.
        strcpy(numberMsg.payload, to_string(file_count).c_str());
        returnQueue.enqueue(numberMsg);
    }
}

/*
 * createPut(void)
 * The process for the Put Child; Creates a file by the name of the key value in the Message
 * the child receives. The contents of the file are the payload.
 */
void * createPut(void * p)
{
    Message putMsg;
    while(true)
    {
        putMsg = putQueue.dequeue();

        cout << "Put server received message #" << putMsg.id << ", key = " << putMsg.key << ", payload = " << putMsg.payload << endl;

        if(putMsg.command == 'Q')
        {
            returnQueue.enqueue(putMsg);
            pthread_exit(0);
        }

        if(access(putMsg.key, F_OK ) != -1) //check to see if a file already exists by the name of the message key
        {
            strcpy(putMsg.payload, duplicateMsg.c_str());
            returnQueue.enqueue(putMsg);
        }
        else
        {
            fstream putStream; //create an fstream for writing to the keyfile.

            putStream.open(putMsg.key, ios::app); //open the file for writing.

            //If we can't open the file, print the error and return the error msg to handler.
            if(!putStream)
            {
                cout << "Can't open the " << putMsg.key << " file!" << endl;
                strcpy(putMsg.payload, errorMessage.c_str());
                returnQueue.enqueue(putMsg);
            }
            else //otherwise write the payload to the file and send "Okay" to the return handler.
            {
                putStream << putMsg.payload << endl;
                putStream.close();  //close the fstream.
                strcpy(putMsg.payload, okayMsg.c_str());
                returnQueue.enqueue(putMsg);
            }
        }
    }
}


/*
 * createSearch(void)
 * This method handles the functionality of the Search Child. Searches for a file by the name of the
 * passed in message. Returns the text found within the file--if found--if not found, tell the parent
 * the file was not found.
 */
void * createSearch(void * p)
{
    Message searchMsg;

    fstream searchStream;
    string payload;
    while(true)
    {
        searchMsg = searchQueue.dequeue();
        cout << "Search server received message #" << searchMsg.id << ", key = " << searchMsg.key << ", payload = " << searchMsg.payload << endl;
        if (searchMsg.command == 'Q')
        {
            returnQueue.enqueue(searchMsg);
            pthread_exit(0);
        }


        searchStream.open(searchMsg.key, ios::in);
        if (!searchStream)  //otherwise we couldn't find/open the file, return the "not found" message.
        {
            strcpy(searchMsg.payload, notFoundString.c_str());
            returnQueue.enqueue(searchMsg);
            searchStream.close();

        }
        else    //if the file opens, it must be present, return the contents of the file.
        {
            getline(searchStream, payload);
            strcpy(searchMsg.payload, payload.c_str());
            returnQueue.enqueue(searchMsg);
            searchStream.close();
        }
    }
}

/**
 * createReturn(void)
 * Purpose: Handles messages that are on the returnQueue from the other child threads. Displays appropriate
 *          messages to the console.
 * @param p
 * @return
 */
void * createReturn(void * p)
{
    Message returnMsg;  // This will hold the popped messages.
    string childType;   // This will hold the string of the type of message we received--which tells us the child's type.
    int quitCounter = 0;// Keep track of the "Q"uits we have received.
    while(true) // Run until we get a Quit command.
    {
        returnMsg = returnQueue.dequeue();
        // Set the type of the child based on the received message.
        switch (returnMsg.command)
        {
            case 'N':
                childType = "Number";
                break;
            case 'P':
                childType = "Put";
                break;
            case 'S':
                childType = "Search";
                break;
            case 'Q':
                quitCounter++;
                if(quitCounter >= 3)
                {
                    pthread_exit(0);
                }
            default:
                break;

        }
        // Report the received message to the console.
        write(connection, (char*)&returnMsg, sizeof(Message));
        cout << childType << " reported to Return server: " << returnMsg.payload << endl;
    }
}

/**
 * Initializes the 4 threads (put, search, number, and return) that will handle messages from the Client.
 */
void initializePThreads()
{

    if     (pthread_create(&putThread, NULL, createPut, NULL) != 0 ||
            pthread_create(&searchThread, NULL, createSearch, NULL) != 0 ||
            pthread_create(&numberThread, NULL, createNumber, NULL) != 0 ||
            pthread_create(&returnThread, NULL, createReturn, NULL) != 0)
    {
        perror("Error on creating threads!");
        exit(0);
    }
}

/*
 * openPipes(int[])
 * Purpose: Opens all pipes used in the program. In this project, there is only one pipe to open.
 */
int openPipes(int loggerPipe[])
{
    int returnInt = 0;  //This will tell us if the pipe opening failed or not.

    //Open all the pipes--if there is an error--report it.

    if(pipe(loggerPipe) == -1)
    {
        returnInt = -1;
    }

    return returnInt;
}


